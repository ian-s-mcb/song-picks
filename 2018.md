

### Legend
* 🔖 - Album review is currently incomplete
* 🎤 - Has vocals (on mostly instrumental album)
* 🥉 - Rating: iffy
* 🥈 - Rating: maybe
* 🥇 - Rating: excellent

## Picks
* Tom Misch - Geography 🔖
* FKJ - French Kiwi Juice 🔖
  * 🥉 02 Skyline
  * 🥉 03 Better give u up
  * 🥇 04 Go back home
  * 🥈 05 Vibin' out
  * 🥉 08 Die with a smile
  * 🥉 09 Lying together (interlude)
  * 🥇 10 Lying together
  * 🥉 11 Joy
* Vulfpeck - Mit Peck EP 🔖
* Vulfpeck - Vollmilch EP 🔖
* Vulfpeck - My First Car EP 🔖
* Vulfpeck - Fugue State EP 🔖
* Vulfpeck - Thrill of the Arts 🔖
* Vulfpeck - The Beautiful Game 🔖
  * 🥇 10 Cory wong
* Vulfpeck - Mr Finish Line 🔖
* Delvon Lamarr Organ Trio - Close But No Cigar 🔖
  * 🥈 01 Concussion
  * 🥇 02 Little booker t
  * 🥈 05 Memphis
  * 🥈 06 Al greenery
  * 🥉 10 Walk on by
* Ratatat - Magnifique 🔖
  * 🥈 02 Cream on chrome
  * 🥉 04 Abrasive
  * 🥉 07 Pricks of brightness
  * 🥈 11 Rome
* Ratatat - LP4
  * 🥈 01 Bilar
  * 🥉 03 Neckbrace
  * 🥉 07 Mahalo
  * 🥉 08 Party with children
  * 🥇 10 Bare feast
  * 🥈 13 Biddang
* The Pharcyde - Labcabincalifornia 🔖
  * 🥇 04 Runnin'
* The Pharcyde - Bizarre Ride II The Pharcyde 🔖
  * 🥈 08 On the dl
  * 🥉 11 Ya mama
  * 🥉 12 Passing me by
  * 🥉 15 Pack the pipe
* Ludovico Einaudi - Divenire 🔖
  * 🥇 06 Primavera
  * 🥈 07 Oltremare
* Ludovico Einaudi - Nightbook
  * 🥈 02 Lady labyrinth
  * 🥇 03 Nightbook
  * 🥈 04 Indaco
  * 🥈 05 The snow prelude n. 15
  * 🥉 06 Eros
  * 🥉 07 The crane dance
  * 🥉 10 Reverie
  * 🥉 11 Bye bye mon amour
  * 🥈 13 Solo
* Ludovico Einaudi - In A Time Lapse
  * 🥈 01 Corale
  * 🥈 02 Time lapse
  * 🥇 03 Life
  * 🥈 04 Walk
  * 🥈 05 Discovery at night
  * 🥈 06 Run
  * 🥇 07 Brothers
  * 🥇 09 Two trees
  * 🥉 10 Newton's cradle
  * 🥉 11 Waterways
  * 🥇 12 Experience
  * 🥈 13 Underwood
  * 🥉 14 Burning
* Emancipator - Dusk to Dawn 🔖
  * 🥇 01 Minor cause
  * 🥉 02 Valhalla
  * 🥉 03 Merlion
  * 🥈 04 Outlaw
  * 🥉 05 Dusk to dawn
  * 🥉 06 The way
  * 🥈 07 Afterglow
  * 🥉 08 Eve ii
  * 🥉 09 Natural cause
  * 🥉 09 Natural cause
* Emancipator - Seven Seas 🔖
  * 🥉 01 All in here
  * 🥉 02 Seven seas feat. madelyn grant 🎤
  * 🥈 03 1993
* Dinosaur Jr. - Give A Glimpse Of What Yer Not 🔖
  * 🥈 03 Be a part
  * 🥈 04 I told everyone
  * 🥈 08 Lost all day
  * 🥇 09 Knocked around
* Dinosaur Jr. - I Bet On Sky 🔖
  * 🥈 02 Watch the corners
* Dinosaur Jr. - Beyond 🔖
  * 🥇 02 Crumble
  * 🥇 03 Pick me up
  * 🥉 04 Back to your heart
  * 🥇 05 This is all i came to do
  * 🥈 07 It's me
  * 🥇 08 We're not alone
* Dinosaur Jr. - Farm 🔖
  * 🥇 01 Pieces
  * 🥉 02 I want you to know
  * 🥈 03 Ocean in the way
  * 🥇 04 Plans
  * 🥇 08 Said the people
  * 🥇 10 See you
  * 🥉 11 I don't wanna go there
* Akala - DoubleThink 🔖
  * 🥇 12 Find no enemy
* Protomartyr - Consolation EP
  * 🥈 03 Wheel of fortune (feat. kelley deal)
* Darwin Deez - Double Down
  * 🥇 02 The mess she made
  * 🥉 07 Melange mining co.
  * 🥈 09 The other side
  * 🥈 10 Right when it rains
  * 🥉 11 The missing i wanna do
* Alice Smith - For Lovers, Dreamers, & Me 🔖
  * 🥈 04 New religion
  * 🥇 08 Know that i
* Alice Smith - She
  * 🥈 01 Cabaret prelude
  * 🥈 02 Cabaret
  * 🥈 04 Another love
  * 🥈 05 The one
  * 🥉 06 Shot
  * 🥈 07 Loyalty
  * 🥇 09 Fool for you
  * 🥇 10 Be easy
  * 🥇 11 She
* Dumbfoundead - DFD
  * 🥉 01 Town
  * 🥇 03 Cool and calm
  * 🥉 04 Green
  * 🥈 06 Cell phone (feat. breezy lovejoy & wax)
  * 🥈 09 Son of a gun (feat. matik)
  * 🥈 12 Tour up (feat. wax)
  * 🥇 13 Are we there yet
* Porches - The House
  * 🥈 02 Find me
  * 🥇 04 Now the water
  * 🥇 05 Country
  * 🥈 06 By my side
  * 🥉 07 Akeren
  * 🥈 08 Anymore
  * 🥉 09 Wobble
  * 🥇 10 Goodbye
  * 🥈 12 W longing
  * 🥉 14 Anything u want
* Porches - Pool
  * 🥇 01 Underwater
  * 🥈 02 Braid
  * 🥇 03 Be apart
  * 🥉 04 Mood
  * 🥇 08 Glow
  * 🥈 09 Car
  * 🥈 10 Shaver
  * 🥉 11 Shape
  * 🥉 12 Security
* Oddisee - Instrumental Mixtape Vol 1
  * 🥉 02 Rhyme and reason
  * 🥈 03 So many places
  * 🥉 04 Gotta pay
  * 🥉 08 Road trip
  * 🥇 12 Stop and listen
  * 🥉 14 Rufus
  * 🥇 15 Ability
  * 🥈 20 Downtown hustle
  * 🥉 22 Gonna be
* Oddisee - Instrumental Mixtape Vol 2
  * 🥉 02 Brother
  * 🥉 03 She remembers
  * 🥈 04 Chuck brown
  * 🥉 05 V3
  * 🥉 09 Trust (remix)
  * 🥉 11 Bring back
  * 🥉 13 Listen
  * 🥈 14 Love at frst sight
  * 🥈 15 Funk for you
  * 🥈 16 Free fall (remix)
  * 🥉 17 Searchin'
  * 🥈 18 Hear my dear
  * 🥉 19 Hottness
  * 🥉 20 The quickening
  * 🥉 21 Soul clap (remix)
  * 🥉 23 Aww yeah
  * 🥇 24 As the world turns
* Tokimonsta - Desiderium EP 🔖
* Tokimonsta - Bedtime Lullabies EP 🔖
* Tokimonsta - Cosmic Intoxication EP 🔖
* Tokimonsta - Half Shadows 🔖
  * 🥈 03 808
  * 🥉 05 Clean slate (feat. gavin turek)
  * 🥉 07 Go with it
  * 🥉 08 Spilling autumn
  * 🥈 09 Sweet williams
  * 🥉 10 Soul to seoul
  * 🥇 12 Waiting for the break of dawn
* Tokimonsta - Midnight Menu 🔖
  * 🥉 02 Sweet day
  * 🥉 09 Simple reminder
  * 🥈 12 Bready soul
  * 🥉 13 Lovely soul
* Flume - Skin Companion EP I 🔖
* Flume and Chet Faker - Lockjaw EP 🔖
* Gil Scott-Heron - Small Talk At 125th and Lenox 🔖
* Gil Scott-Heron - It's Your World 🔖
* John Fahey - The Transfiguration of Blind Joe Death 🔖
* John Fahey - The Great San Bernardino Birthday Party ... 🔖
* The Inmates - First Offence 🔖
* The Bouncing Souls - The Good The Bad And The Argyle
  * 🥈 02 The guest
  * 🥈 04 Joe lies (when he cries)
  * 🥈 05 Some kind of wonderful
  * 🥉 07 Old school
  * 🥉 08 Candy
  * 🥉 09 Neurotic
  * 🥉 11 Deadbeats
  * 🥉 12 I know what boys like
* The Bouncing Souls - The Bouncing Souls 🔖
* The Inmates - Heatwave In Alaska 🔖
* NOFX - Pump Up The Valuum 🔖
* NOFX - Punk in Drublic
  * 🥇 01 Linoleum
  * 🥇 02 Leave it alone
  * 🥈 03 Dig
  * 🥇 04 The cause
  * 🥇 05 Don't call me white
  * 🥉 10 Dying degree
  * 🥈 11 Fleas
  * 🥈 12 Lori Meyers
  * 🥉 14 Punk guy
  * 🥉 16 Reeko
  * 🥉 17 Scavenger type
* Boards of Canada - Tomorrow's Harvest
  * 🥇 02 Reach for the dead
  * 🥉 03 White cyclosa
  * 🥇 08 Sick times
  * 🥉 09 Collapse
  * 🥉 11 Split your infinities
  * 🥇 13 Nothing is real
  * 🥉 14 Sundown
  * 🥉 15 New seeds
  * 🥉 17 Semena mertvykh
* Gil Scott-Heron - Pieces of a Man 🔖
* Gil Scott-Heron - Winter In America 🔖
* Slow Magic - Triangle
  * 🥉 02 Toddler tiger
  * 🥉 04 Youths
  * 🥉 05 Moon
  * 🥉 07 Sorry safari
  * 🥉 08 Music
* The Inmates - Shot In The Dark 🔖
* John Fahey - The Legend of Blind Joe Death 🔖
  * 🥇 09 Sligo river blues
* Helen Jane Long - Embers 🔖
  * 🥇 01 Embers
  * 🥇 03 Finding
  * 🥈 06 Mirror
  * 🥇 10 Bells
* Helen Jane Long - Intervention 🔖
  * 🥇 05 Rainbow
  * 🥉 09 Passes
  * 🥉 10 Goodnight
* Twin Shadow - Confess 🔖
* Twin Shadow - Forget
  * 🥉 01 Tyrant destroyed
  * 🥈 02 When we're dancing
  * 🥈 03 I can't wait
  * 🥇 04 Shooting holes at the moon
  * 🥇 05 At my heels
  * 🥈 06 Yellow balloon
  * 🥈 07 Tether beat
  * 🥇 08 Castles in the snow
  * 🥈 09 For now
  * 🥇 10 Slow
  * 🥇 11 Forget
* Cat Power - The Greatest 🔖
* Cat Power - You Are Free 🔖
* Cat Power - Jukebox 🔖
* Cat Power - Myra Lee
  * 🥈 03 Great expectations
  * 🥈 11 Not what you want
* Cat Power - Dear Sir 🔖
  * 🥇 02 Rockets
* Kristofer Maddigan - Cuphead - Original Soundtrack 🔖
  * 🥇 07 Botanic panic
  * 🥇 10 Threatin' zeppelin
  * 🥈 11 Treetop trouble
  * 🥇 12 Ruse of an ooze
  * 🥇 13 Floral fury
  * 🥈 14 Inkwell isle one (piano)
  * 🥈 15 Clip join calamity
  * 🥈 22 Fiery frolic
  * 🥈 24 The mausolem
  * 🥇 25 Legendary ghost
  * 🥇 27 Victory tune
  * 🥇 31 High score
  * 🥇 35 Porkrind's shop
  * 🥈 46 All bets are off
  * 🥇 47 Inkwell hell
  * 🥇 49 Inkwell hell (piano)
  * 🥈 54 The end
  * 🥈 56 Closing credits
* Guided by Voices - Space Gun
  * 🥇 01 Space gun
  * 🥈 04 Ark technician
  * 🥉 05 See my field
  * 🥉 06 Liar's box
  * 🥈 07 Blink blank
  * 🥉 09 Hudson rake
  * 🥉 10 Sport component national
  * 🥈 11 I love kangaroos
* Volt Age - Pleasure Operator
  * 🥇 01 Welcome to the volt age
  * 🥉 03 Dawn hunter
  * 🥈 04 Sensual overdrive
  * 🥉 05 Theme 1000
* Volt Age - Misc Singles
  * 🥉 01 Supreme delight
  * 🥇 01 Volt's theme
  * 🥈 01 Wunderbar
* 憂鬱 YU-UTSU - S/T
  * 🥉 01 Sun
  * 🥈 02 Moon
  * 🥈 03 Slow
  * 🥈 04 Clear
  * 🥉 05 Prime (YU-UTSU & hello meteor)
  * 🥈 06 Nu (yu-utsu & aima lecia)
  * 🥈 07 Endurance (yu-utsu & oddling)
  * 🥇 08 Dive (yu-utsu & a.l.i.s.o.n.)
  * 🥈 09 Curious (yu-utsu & oddling)
* Cat Power - What Would The Community Think
  * 🥇 04 Nudes as the news
  * 🥉 05 They tell me
  * 🥉 07 Fate of the human carbine
  * 🥉 12 The coat is always on
* Apparat - The Devil's Walk
  * 🥉 01 Sweet unrest
  * 🥉 02 Song of los
  * 🥇 03 Black water
  * 🥈 04 Goodbye
  * 🥇 06 The soft voices die
  * 🥉 07 Escape
  * 🥇 08 Ash black veil
  * 🥉 09 A band in the void
* Ramin Djawadi - Game of Thrones Music From the HBO Series 🔖
* Flume - Flume (Deluxe Edition)
  * 🥇 01 Intro (feat. stalley)
  * 🥉 02 Space cadet (feat. ghostface killah & autre ne veut)
  * 🥇 03 Instane (feat. moon holiday & killer mike)
  * 🥇 05 Holdin' on (feat. freddie gibbs)
  * 🥈 11 Hyperparadise (flume remix)
  * 🥇 12 A baru in new york (flume soundtrack version)
  * 🥈 13 Zimbabwe (flume remix)
  * 🥇 18 Insane (feat. moon holiday) [l d r u remix]
* Flume - Flume
  * 🥉 01 Sintra
  * 🥈 02 Holdin' on
  * 🥇 03 Left alone (feat. Chet Faker)
  * 🥈 04 Sleepless (feat. jezzabell doran)
  * 🥇 05 On top (feat. T-Shirt)
  * 🥇 08 Change
  * 🥇 09 Ezra
  * 🥈 11 Space cadet
  * 🥉 12 Bring you down (feat. george maple)
  * 🥉 13 Warm thoughts
* Flume - Skin Companion EP II
  * 🥈 02 Weekend (feat. moses sumney)
  * 🥇 03 Depth charge
  * 🥉 04 Fantastic (feat. Dave Glass Animals)
* Cachao y su Ritmo Caliente - Descargas - Cuban Jam Sessions
  * 🥈 01 Descarga cubana
  * 🥉 02 Goza mi trompeta
  * 🥈 03 Cojele el golpe
  * 🥈 04 Trombon criollo
  * 🥈 05 Malanga amarilla
  * 🥉 06 Pamparana
  * 🥇 07 Oye me tres montuno
  * 🥉 08 Controversia en metales
  * 🥈 09 A gozar timbero
  * 🥇 11 Estudio en trompeta
  * 🥇 12 Guajeo de saxos
* Mogwai - Happy Songs For Happy People
  * 🥉 01 Hunted by a freak
  * 🥉 03 Kids will be skeletons
  * 🥈 06 Ratts of the capital
  * 🥈 07 Golden porsche
  * 🥇 08 I know you are but what am i
  * 🥉 09 Stop coming to my house
  * 🥉 10 Sad dc
* Moodorama - My Name is Madness
  * 🥈 01 Space cowboy
  * 🥈 02 Nova star
  * 🥉 03 Summer's ocean
  * 🥈 05 The journey
  * 🥈 06 Fly me to the moon
  * 🥉 07 Ir facil
  * 🥉 09 My name is madness
  * 🥈 10 Beatzekatze
  * 🥇 11 Too late
  * 🥈 12 Solitude
  * 🥈 14 Director's cut
* Slow Magic - How To Run Away
  * 🥈 01 Still life
  * 🥉 02 Girls
  * 🥇 03 Waited 4 u
  * 🥇 04 Hold still
  * 🥇 06 Let u go
  * 🥈 07 Manhattan
  * 🥈 10 Closer
* Toro Y Moi - Anything In Return
  * 🥇 03 So many details
  * 🥇 04 Rose quartz
  * 🥈 05 Touch
  * 🥈 06 Cola
  * 🥇 08 High living
  * 🥇 09 Grown up calls
  * 🥉 11 Day one
  * 🥇 13 How's it wrong
* Toro Y Moi - Underneath The Pine
  * 🥉 01 Intro chi chi
  * 🥉 02 New beat
  * 🥇 03 Go with you
  * 🥈 04 Divina
  * 🥉 05 Before i'm done
  * 🥇 06 Got blinded
  * 🥉 07 How I know
  * 🥉 09 Still sound
  * 🥉 11 Elise
* Big Thief - Capacity
  * 🥇 01 Pretty things
  * 🥇 02 Shark smile
  * 🥈 03 Capacity
  * 🥇 04 Watering
  * 🥇 05 Coma
  * 🥈 06 Great white shark
  * 🥇 07 Mythological beauty
  * 🥈 08 Object
  * 🥈 09 Haley
  * 🥇 10 Mary
  * 🥇 11 Black diamonds
* Joy Division - Closer
  * 🥇 02 Isolation
  * 🥇 03 Passover
  * 🥉 04 Colony
  * 🥈 05 A means to an end
  * 🥈 06 Heart and soul
  * 🥈 07 Twenty four hours
  * 🥇 09 Decades
* Joy Division - Unknown Pleasures
  * 🥇 01 Disorder
  * 🥇 02 Day of the lords
  * 🥈 03 Candidate
  * 🥈 04 Insight
  * 🥇 05 New dawn fades
  * 🥈 06 She's lost control
  * 🥇 07 Shadowplay
  * 🥇 09 Interzone
  * 🥉 10 I remember nothing
* Protomartyr - Relatives In Descent
  * 🥈 01 A private understanding
  * 🥉 02 Here is the thing
  * 🥇 03 My children
  * 🥇 05 The chuckler
  * 🥈 06 Windsor hum
  * 🥉 07 Don't go to anacita
  * 🥉 08 Up the tower
  * 🥇 09 Night-blooming cereus
  * 🥈 10 Male plague
  * 🥉 11 Corpses in regalia
  * 🥈 12 Half sister
